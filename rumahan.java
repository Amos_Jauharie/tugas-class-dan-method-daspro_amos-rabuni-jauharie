import java.util.Scanner;
/**
 * Nama : Amos Rabuni Jauharie
 * NIM  : H1051181040
 * Tugas: Membuat kode menggunakan Method dan Class
 */
class rumah{
 int luas, tinggi;
 String warna;    
 // method output
  void input(){
   Scanner s=new Scanner(System.in);
   Scanner o=new Scanner(System.in);
   Scanner t=new Scanner(System.in);
   System.out.println("");
   System.out.println("");
   System.out.println("");
   System.out.println("(Dalam meter kubik)");
   System.out.print("Luas   : ");
   luas=s.nextInt();
   System.out.println("(Dalam tinggi lantai)");
   System.out.print("Tinggi : ");
   tinggi=o.nextInt();
   System.out.println("(Warna dinding rumah)");
   System.out.print("Warna  : ");
   warna=t.nextLine();
   System.out.println("");
   System.out.println("");
   System.out.println("");
    }    
    // Method output
    void output(){
     System.out.println("");
     System.out.println("");
     System.out.println("");
     System.out.println("");
     System.out.println("======RUMAH ANDA======");
     System.out.print("Luas rumah   : "+luas);
     System.out.println(" meter kubik");
     System.out.print("Tinggi rumah : "+tinggi);
     System.out.println(" lantai");
     System.out.println("Warna rumah  : "+warna);
     System.out.println("======================");
     System.out.println("");
     System.out.println("");
     System.out.println("");
    }
}
public class rumahan {
 public static void main(String[] args) {
  //membuat objek baru
   rumah objek1 = new rumah();
    // mengakses dan menjalankan method input
    objek1.input();        
    // mengakses dan menjalankan method output
    objek1.output();
    }    
}